﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace pruebaSigma.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartamentosCiudadesController : ControllerBase
    {
        // GET: api/<DepartamentosCiudadesController>
        [HttpGet]
        public async Task<string> Get()
        {
            return await Consultar();
        }

        public async Task<string> Consultar()
        {
            try
            {
                HttpClient client;
                client = new HttpClient();
                client.MaxResponseContentBufferSize = 256000;
                string url = "https://sigma-studios.s3-us-west-2.amazonaws.com/test/colombia.json";
                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();

                    return content;
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
