﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pruebaSigma.Models
{
    public class SigmatestContext : DbContext
    {
        public SigmatestContext(DbContextOptions<SigmatestContext> options) : base(options)
        {

        }


        public DbSet<ContactsEntity> contacts { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ContactsEntity>()
                .HasKey(c => c.id);


        }
    }
}
