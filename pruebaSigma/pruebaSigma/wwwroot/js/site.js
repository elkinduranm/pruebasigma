﻿window.onload = function () {
    getDepartamento();
}
var respuesta;
function guardarContact() {
    var validacion = 1;
    if ($("#nombre").val() == null || $("#nombre").val() == '') {
        $("#validanombre").text("Es requerido el nombre");
        $('#modal').modal().hide();
    }
    if ($("#departamentos").val() == null || $("#departamentos").val() == '') {
        $("#validadepartamento").text("Es requerido el departamento");
        $('#respuesta').modal('hide');
    }
    if ($("#ciudades").val() == null || $("#ciudades").val() == '') {
        $("#validaciudad").text("Es requerido la ciudad");
        $('#respuesta').modal('hide');
    }
    var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    if (!emailRegex.test($("#correo").val())) {

        $("#validacorreo").text("Es requerido el correo");
        $('#respuesta').modal('hide');
    }
    var datos = {
        id: 0,
        name: $("#nombre").val(),
        email: $("#correo").val(),
        state: $("#departamentos").val(),
        city: $("#ciudades").val()
    };

    html = "Procesando...";
    $("#proceso").html(html);
    $.post("api/Contacts", datos, function (data, status) {
        console.log(data);
        if (status == "success") {
           html="Contacto Guardado";
        } else {
            html ="Error: No se pudo guardar el contacto";
        }
        $("#proceso").html(html);
    });
}

function getDepartamento() {
    $.get("api/DepartamentosCiudades", function (data, status) {
        respuesta = JSON.parse(data);
        var departamentos = Object.keys(respuesta);
        for (var i = 0; i < departamentos.length; i++) {

            $("#departamentos").append('<option > ' + departamentos[i] +'</option>');
        }
    });
}

$("#departamentos").change(function () {
    var departamentoSelect = $("#departamentos").val();
    
    var cd = respuesta[departamentoSelect];
    var linea = 0;
    for (var i = 0; i < cd.length; i++) {
        console.log(cd[i]);
        $("#ciudades").append('<option > ' + cd[i] + '</option>');
        linea = i;
    }
    $("#ciudades > option[value='" + cd[linea] + "']").attr("selected", true);
    console.log(cd);


});

function getCiudad() {
    console.log("---");
    var dep = $("#departamentos").val();
    console.log(dep);
}
